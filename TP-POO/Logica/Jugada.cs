﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Jugada
    {
        public int MontoApostado { get; set; }
        public int MontoGanado { get; set; }
        public DateTime Fecha { get; set; }
        public List<Apuesta> Apuestas { get; set; }

        /// <summary>
        /// Crea y carga una nueva Apuesta en Apuestas, y la devuelve para guardarla.
        /// </summary>
        /// <param name="apostadoEnCadaNumero"></param>
        /// <returns></returns>
        public List<Apuesta> DevolverListaApuestas(int[] apostadoEnCadaNumero)
        {
            List<Apuesta> apuesta = new List<Apuesta>();

            for (int i = 0; i < apostadoEnCadaNumero.Length; i++)
            {
                if (apostadoEnCadaNumero[i] != 0)
                {
                    Apuesta nuevaApuesta = new Apuesta
                    {
                        NúmeroApostado = i,
                        CantidadApostada = apostadoEnCadaNumero[i]
                    };

                    apuesta.Add(nuevaApuesta);
                }
            }

            return apuesta;
        }
    }
}