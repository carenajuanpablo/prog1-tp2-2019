﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Movimiento
    {
        public DateTime FechaDeCarga { get; set; }
        public int MontoCargado { get; set; }
    }
}