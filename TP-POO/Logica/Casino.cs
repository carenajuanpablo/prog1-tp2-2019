﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Casino
    {
        public Usuario UserLoggued { get; set; } //VARIABLES EN CASTELLANO O INGLES, NO AMBAS (PREFERENTE INGLES)

        //readonly => variable que se puede escribir una sola vez.
        readonly string pathListaDeAdministradores = Path.GetFullPath("..\\..\\..\\Archivos\\ListaDeAdministradores.txt");
        readonly string pathListaDeJugadores = Path.GetFullPath("..\\..\\..\\Archivos\\ListaDeJugadores.txt");
        
        public List<Administrador> ListaDeAdministradores { get; set; }
        public List<Jugador> ListaDeJugadores { get; set; }

        public Casino()
        {
            ListaDeAdministradores = LeerListaDeAdmin();
            ListaDeJugadores = LeerListaDeJugador();
        }

        /// <summary>
        /// Devuelve el Usuario logueado.
        /// </summary>
        /// <returns></returns>
        public Usuario ObtenerUsuarioLogueado()
        {
            //SI LA PROPIEDAD ES PUBLICA, EL METODO NO TIENE SENTIDO
            return UserLoggued;
        }

        /// <summary>
        /// Busca el Usuario según el email y la contraseña ingresada.
        /// Devuelve un Usuario o null (si no se encuentra el Usuario).
        /// </summary>
        /// <param name="email"></param>
        /// <param name="pass"></param>
        /// <returns></returns>
        public Usuario Login(string email, string pass)
        {
            List<Usuario> Usuarios = ObtenerUsuarios();
            Usuario UsuarioEncontrado = null;

            foreach (var item in Usuarios)
            {
                if ((item.Email == email) && (item.Pass == pass))
                {
                    UsuarioEncontrado = item;
                    break;
                }
            }

            Usuario UserLogged = new Usuario();
            UserLoggued = UsuarioEncontrado;

            return UsuarioEncontrado;
        }

        /// <summary>
        /// Lee el archivo ListaDeAdministradores.
        /// </summary>
        /// <returns></returns>
        public List<Administrador> LeerListaDeAdmin()
        {
            if (!File.Exists(pathListaDeAdministradores))
            {                
                TextWriter archivo = new StreamWriter(pathListaDeAdministradores);
                List<Administrador> Lista = new List<Administrador>();
                archivo.Close();
                return Lista;
            }
            else
            {
                using (StreamReader archivo = new StreamReader(pathListaDeAdministradores))
                {
                    string JsonContenido = archivo.ReadToEnd();
                    List<Administrador> Lista = JsonConvert.DeserializeObject<List<Administrador>>(JsonContenido);

                    archivo.Close();

                    if (Lista == null)
                        Lista = new List<Administrador>();
                    return Lista;
                }
            }
        }

        /// <summary>
        /// Lee el archivo ListaDeJugadores.
        /// </summary>
        /// <returns></returns>
        public List<Jugador> LeerListaDeJugador()
        {
            if (!File.Exists(pathListaDeJugadores))
            {
                TextWriter archivo = new StreamWriter(pathListaDeJugadores);
                List<Jugador> Lista = new List<Jugador>();
                archivo.Close();
                return Lista;
            }
            else
            {
                using (StreamReader archivo = new StreamReader(pathListaDeJugadores))
                {
                    string JsonContenido = archivo.ReadToEnd();
                    List<Jugador> Lista = JsonConvert.DeserializeObject<List<Jugador>>(JsonContenido);

                    archivo.Close();

                    if (Lista == null)
                        Lista = new List<Jugador>();
                    return Lista;
                }
            }
        }

        /// <summary>
        /// Guarda el nuevo jugador creado en la ListaDeJugadores y llama a ActualizarArchivo()
        /// </summary>
        /// <param name="_nombre"></param>
        /// <param name="_mail"></param>
        /// <param name="_pass"></param>
        public void GuardarNuevoJugador(string _nombre, string _mail, string _pass)
        {
            Jugador nuevoJugador = new Jugador
            {
                Codigo = ListaDeJugadores.Count + 1,
                Nombre = _nombre,
                Email = _mail,
                Pass = _pass,
            };

            ListaDeJugadores.Add(nuevoJugador);

            ActualizarArchivo();
        }

        /// <summary>
        /// Actualiza el Archivo Json
        /// </summary>
        public void ActualizarArchivo()
        {
            using (StreamWriter archivo = new StreamWriter(pathListaDeJugadores))
            {
                string JsonContenido = JsonConvert.SerializeObject(ListaDeJugadores);
                archivo.Write(JsonContenido);
                archivo.Close();
            }
        }

        /// <summary>
        /// Se comunica con Jugador para actualizar el saldo
        /// </summary>
        /// <param name="saldo"></param>
        public void ActualizarSaldo(int saldo)
        {
            Jugador jugador = UserLoggued as Jugador;
            jugador.ActualizarSaldo(saldo);
            ActualizarArchivo();
        }

        /// <summary>
        /// Se comunica con Jugador para crear una nueva Jugada.
        /// </summary>
        /// <param name="apostado"></param>
        /// <param name="ganado"></param>
        /// <param name="apostadoEnCadaNumero"></param>
        public void GuardarJugada(int apostado, int ganado, int[] apostadoEnCadaNumero)
        {
            Jugador jugador = UserLoggued as Jugador;
            //SI ESTO RETORNARA OTRO TIPO DE USUARIO, DARIA UNA EXCEPCION PORQ JUGADOR SERIA NULL
            jugador.CargarJugada(apostado, ganado, apostadoEnCadaNumero);
        }
        
        /// <summary>
        /// Devuelve el Saldo del jugador logueado.
        /// </summary>
        /// <returns></returns>
        public int ObtenerSaldoJugador()
        {
            Jugador jugador = UserLoggued as Jugador;
            //SI ESTO RETORNARA OTRO TIPO DE USUARIO, DARIA UNA EXCEPCION PORQ JUGADOR SERIA NULL
            return jugador.ObtenerSaldo();
        }

        /// <summary>
        /// Devuelve una lista con todos los usuarios existentes.
        /// </summary>
        /// <returns></returns>
        private List<Usuario> ObtenerUsuarios()
        {
            List<Usuario> res = new List<Usuario>();
            res.AddRange(ListaDeAdministradores);

            foreach (var item in ListaDeJugadores)
            {
                if (item.Activo)
                {
                    res.Add(item);
                }
            }
            
            return res;
        }

        public List<Jugador> ObtenerJugadores()
        {
            //ESTE METODO Y EL DE ARRIBA SON IGUALES, PODRIA MEJORARSE PARA NO DUPLICAR CODIGO
            List<Jugador> res = new List<Jugador>();
            foreach (var item in ListaDeJugadores)
            {
                if (item.Activo)
                {
                    res.Add(item);
                }
            }

            return res;
        }

        /// <summary>
        /// Se comunica con Jugador para crear un nuevo movimiento.
        /// </summary>
        /// <param name="saldo"></param>
        public void CrearUnNuevoMovimiento(int saldo)
        {
            Jugador jugador = UserLoggued as Jugador;
            jugador.CargarMovimiento(saldo);
        }

        public void ModificarJugador(string nombre, string email, string pass, int codigo)
        {
            foreach (var item in ListaDeJugadores)
            {
                if (item.Codigo == codigo)
                {
                    item.Nombre = nombre;
                    item.Email = email;
                    item.Pass = pass;
                }
            }

            ActualizarArchivo();
        }

        public Jugador ObtenerJugadorPorCodigo(int codigo)
        {
            Jugador jugadorEncontrado = new Jugador();

            foreach (Jugador jugador in ListaDeJugadores)
            {
                if (jugador.Codigo == codigo)
                {
                    jugadorEncontrado = jugador;
                }
            }

            return jugadorEncontrado;
        }

        public void EliminarJugador(int codigo)
        {
            foreach (var item in ListaDeJugadores)
            {
                if (item.Codigo == codigo)
                {
                    item.Activo = false;
                }
            }

            ActualizarArchivo();
        }
    }
}