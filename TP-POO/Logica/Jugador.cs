﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Logica
{
    public class Jugador : Usuario
    {
        public List<Jugada> ListaDeJugadas { get; set; }
        public int Saldo { get; set; }
        public List<Movimiento> ListaDeMovimientos { get; set; }
        public bool Activo { get; set; }

        public Jugador()
        {
            ListaDeJugadas = new List<Jugada>();
            Saldo = 0;
            ListaDeMovimientos = new List<Movimiento>();
            Activo = true;
        }
        
        /// <summary>
        /// Actualiza el saldo del Jugador, despues de una jugada o despues de cargar Saldo a su cuenta.
        /// </summary>
        /// <param name="saldo"></param>
        public void ActualizarSaldo(int saldo)
        {
            //PODRIA VALIDAR Q EL SALDO Q QUIERE INGRESAR SEA > 0
            Saldo = Saldo + saldo;
        }
        
        /// <summary>
        /// Crea y carga una nueva Jugada en ListaDeJugadas.
        /// </summary>
        /// <param name="apostado"></param>
        /// <param name="ganado"></param>
        /// <param name="apostadoEnCadaNumero"></param>
        /// <returns></returns>
        public void CargarJugada(int apostado, int ganado, int[] apostadoEnCadaNumero)
        {
            Jugada nuevaJugada = new Jugada
            {
                MontoApostado = apostado,
                MontoGanado = ganado,
                Fecha = DateTime.Now
            };

            nuevaJugada.Apuestas = nuevaJugada.DevolverListaApuestas(apostadoEnCadaNumero);

            ListaDeJugadas.Add(nuevaJugada);
        }

        /// <summary>
        /// Devuelve el saldo del jugador.
        /// </summary>
        /// <returns></returns>
        public int ObtenerSaldo()
        {
            //SI LA PROPIEDAD ES PUBLICA EL METODO NO TIENE SENTIDO
            return Saldo;
        }

        public void CargarMovimiento(int saldo)
        {
            Movimiento nuevoMovimiento = new Movimiento
            {
                FechaDeCarga = DateTime.Today,
                MontoCargado = saldo
            };

            ListaDeMovimientos.Add(nuevoMovimiento);
        }
    }
}