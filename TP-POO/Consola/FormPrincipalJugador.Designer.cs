﻿namespace Consola
{
    partial class FormPrincipalJugador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.jUGARToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jugadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbltxtSaldo = new System.Windows.Forms.Label();
            this.lblSaldo = new System.Windows.Forms.Label();
            this.lbltxt0 = new System.Windows.Forms.Label();
            this.lbltxt1 = new System.Windows.Forms.Label();
            this.lbltxt2 = new System.Windows.Forms.Label();
            this.lbltxt3 = new System.Windows.Forms.Label();
            this.lbltxt6 = new System.Windows.Forms.Label();
            this.lbltxt5 = new System.Windows.Forms.Label();
            this.lbltxt4 = new System.Windows.Forms.Label();
            this.lbltxt9 = new System.Windows.Forms.Label();
            this.lbltxt8 = new System.Windows.Forms.Label();
            this.lbltxt7 = new System.Windows.Forms.Label();
            this.btnJugar = new System.Windows.Forms.Button();
            this.btnCargarSaldo = new System.Windows.Forms.Button();
            this.numericUpDown0 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown9 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSaldo = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSaldo)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jUGARToolStripMenuItem,
            this.jugadasToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(735, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // jUGARToolStripMenuItem
            // 
            this.jUGARToolStripMenuItem.Name = "jUGARToolStripMenuItem";
            this.jUGARToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.jUGARToolStripMenuItem.Text = "JUGAR";
            // 
            // jugadasToolStripMenuItem
            // 
            this.jugadasToolStripMenuItem.Name = "jugadasToolStripMenuItem";
            this.jugadasToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.jugadasToolStripMenuItem.Text = "Jugadas";
            this.jugadasToolStripMenuItem.Click += new System.EventHandler(this.jugadasToolStripMenuItem_Click);
            // 
            // lbltxtSaldo
            // 
            this.lbltxtSaldo.AutoSize = true;
            this.lbltxtSaldo.BackColor = System.Drawing.SystemColors.Control;
            this.lbltxtSaldo.Location = new System.Drawing.Point(79, 65);
            this.lbltxtSaldo.Name = "lbltxtSaldo";
            this.lbltxtSaldo.Size = new System.Drawing.Size(46, 13);
            this.lbltxtSaldo.TabIndex = 1;
            this.lbltxtSaldo.Text = "SALDO:";
            // 
            // lblSaldo
            // 
            this.lblSaldo.AutoSize = true;
            this.lblSaldo.Location = new System.Drawing.Point(132, 65);
            this.lblSaldo.Name = "lblSaldo";
            this.lblSaldo.Size = new System.Drawing.Size(13, 13);
            this.lblSaldo.TabIndex = 2;
            this.lblSaldo.Text = "0";
            // 
            // lbltxt0
            // 
            this.lbltxt0.AutoSize = true;
            this.lbltxt0.Location = new System.Drawing.Point(92, 223);
            this.lbltxt0.Name = "lbltxt0";
            this.lbltxt0.Size = new System.Drawing.Size(13, 13);
            this.lbltxt0.TabIndex = 3;
            this.lbltxt0.Text = "0";
            // 
            // lbltxt1
            // 
            this.lbltxt1.AutoSize = true;
            this.lbltxt1.Location = new System.Drawing.Point(209, 154);
            this.lbltxt1.Name = "lbltxt1";
            this.lbltxt1.Size = new System.Drawing.Size(13, 13);
            this.lbltxt1.TabIndex = 4;
            this.lbltxt1.Text = "1";
            // 
            // lbltxt2
            // 
            this.lbltxt2.AutoSize = true;
            this.lbltxt2.Location = new System.Drawing.Point(209, 223);
            this.lbltxt2.Name = "lbltxt2";
            this.lbltxt2.Size = new System.Drawing.Size(13, 13);
            this.lbltxt2.TabIndex = 5;
            this.lbltxt2.Text = "2";
            // 
            // lbltxt3
            // 
            this.lbltxt3.AutoSize = true;
            this.lbltxt3.Location = new System.Drawing.Point(209, 301);
            this.lbltxt3.Name = "lbltxt3";
            this.lbltxt3.Size = new System.Drawing.Size(13, 13);
            this.lbltxt3.TabIndex = 6;
            this.lbltxt3.Text = "3";
            // 
            // lbltxt6
            // 
            this.lbltxt6.AutoSize = true;
            this.lbltxt6.Location = new System.Drawing.Point(353, 301);
            this.lbltxt6.Name = "lbltxt6";
            this.lbltxt6.Size = new System.Drawing.Size(13, 13);
            this.lbltxt6.TabIndex = 9;
            this.lbltxt6.Text = "6";
            // 
            // lbltxt5
            // 
            this.lbltxt5.AutoSize = true;
            this.lbltxt5.Location = new System.Drawing.Point(353, 224);
            this.lbltxt5.Name = "lbltxt5";
            this.lbltxt5.Size = new System.Drawing.Size(13, 13);
            this.lbltxt5.TabIndex = 8;
            this.lbltxt5.Text = "5";
            // 
            // lbltxt4
            // 
            this.lbltxt4.AutoSize = true;
            this.lbltxt4.Location = new System.Drawing.Point(353, 154);
            this.lbltxt4.Name = "lbltxt4";
            this.lbltxt4.Size = new System.Drawing.Size(13, 13);
            this.lbltxt4.TabIndex = 7;
            this.lbltxt4.Text = "4";
            // 
            // lbltxt9
            // 
            this.lbltxt9.AutoSize = true;
            this.lbltxt9.Location = new System.Drawing.Point(520, 301);
            this.lbltxt9.Name = "lbltxt9";
            this.lbltxt9.Size = new System.Drawing.Size(13, 13);
            this.lbltxt9.TabIndex = 12;
            this.lbltxt9.Text = "9";
            // 
            // lbltxt8
            // 
            this.lbltxt8.AutoSize = true;
            this.lbltxt8.Location = new System.Drawing.Point(520, 223);
            this.lbltxt8.Name = "lbltxt8";
            this.lbltxt8.Size = new System.Drawing.Size(13, 13);
            this.lbltxt8.TabIndex = 11;
            this.lbltxt8.Text = "8";
            // 
            // lbltxt7
            // 
            this.lbltxt7.AutoSize = true;
            this.lbltxt7.Location = new System.Drawing.Point(520, 154);
            this.lbltxt7.Name = "lbltxt7";
            this.lbltxt7.Size = new System.Drawing.Size(13, 13);
            this.lbltxt7.TabIndex = 10;
            this.lbltxt7.Text = "7";
            // 
            // btnJugar
            // 
            this.btnJugar.Location = new System.Drawing.Point(280, 394);
            this.btnJugar.Name = "btnJugar";
            this.btnJugar.Size = new System.Drawing.Size(190, 33);
            this.btnJugar.TabIndex = 13;
            this.btnJugar.Text = "JUGAR!";
            this.btnJugar.UseVisualStyleBackColor = true;
            this.btnJugar.Click += new System.EventHandler(this.btnJugar_Click);
            // 
            // btnCargarSaldo
            // 
            this.btnCargarSaldo.Location = new System.Drawing.Point(440, 54);
            this.btnCargarSaldo.Name = "btnCargarSaldo";
            this.btnCargarSaldo.Size = new System.Drawing.Size(125, 28);
            this.btnCargarSaldo.TabIndex = 14;
            this.btnCargarSaldo.Text = "Cargar Saldo";
            this.btnCargarSaldo.UseVisualStyleBackColor = true;
            this.btnCargarSaldo.Click += new System.EventHandler(this.btnCargarSaldo_Click);
            // 
            // numericUpDown0
            // 
            this.numericUpDown0.Location = new System.Drawing.Point(125, 221);
            this.numericUpDown0.Name = "numericUpDown0";
            this.numericUpDown0.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown0.TabIndex = 15;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(269, 152);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown1.TabIndex = 16;
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(269, 221);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown2.TabIndex = 17;
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(269, 299);
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown3.TabIndex = 18;
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.Location = new System.Drawing.Point(409, 299);
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown6.TabIndex = 21;
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.Location = new System.Drawing.Point(409, 221);
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown5.TabIndex = 20;
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Location = new System.Drawing.Point(409, 152);
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown4.TabIndex = 19;
            // 
            // numericUpDown9
            // 
            this.numericUpDown9.Location = new System.Drawing.Point(593, 294);
            this.numericUpDown9.Name = "numericUpDown9";
            this.numericUpDown9.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown9.TabIndex = 24;
            // 
            // numericUpDown8
            // 
            this.numericUpDown8.Location = new System.Drawing.Point(593, 221);
            this.numericUpDown8.Name = "numericUpDown8";
            this.numericUpDown8.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown8.TabIndex = 23;
            // 
            // numericUpDown7
            // 
            this.numericUpDown7.Location = new System.Drawing.Point(593, 152);
            this.numericUpDown7.Name = "numericUpDown7";
            this.numericUpDown7.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown7.TabIndex = 22;
            // 
            // numericUpDownSaldo
            // 
            this.numericUpDownSaldo.Location = new System.Drawing.Point(571, 58);
            this.numericUpDownSaldo.Name = "numericUpDownSaldo";
            this.numericUpDownSaldo.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownSaldo.TabIndex = 25;
            // 
            // FormPrincipalJugador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 450);
            this.Controls.Add(this.numericUpDownSaldo);
            this.Controls.Add(this.numericUpDown9);
            this.Controls.Add(this.numericUpDown8);
            this.Controls.Add(this.numericUpDown7);
            this.Controls.Add(this.numericUpDown6);
            this.Controls.Add(this.numericUpDown5);
            this.Controls.Add(this.numericUpDown4);
            this.Controls.Add(this.numericUpDown3);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.numericUpDown0);
            this.Controls.Add(this.btnCargarSaldo);
            this.Controls.Add(this.btnJugar);
            this.Controls.Add(this.lbltxt9);
            this.Controls.Add(this.lbltxt8);
            this.Controls.Add(this.lbltxt7);
            this.Controls.Add(this.lbltxt6);
            this.Controls.Add(this.lbltxt5);
            this.Controls.Add(this.lbltxt4);
            this.Controls.Add(this.lbltxt3);
            this.Controls.Add(this.lbltxt2);
            this.Controls.Add(this.lbltxt1);
            this.Controls.Add(this.lbltxt0);
            this.Controls.Add(this.lblSaldo);
            this.Controls.Add(this.lbltxtSaldo);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormPrincipalJugador";
            this.Text = "FormPrincipalJugador";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSaldo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem jUGARToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jugadasToolStripMenuItem;
        private System.Windows.Forms.Label lbltxtSaldo;
        private System.Windows.Forms.Label lblSaldo;
        private System.Windows.Forms.Label lbltxt0;
        private System.Windows.Forms.Label lbltxt1;
        private System.Windows.Forms.Label lbltxt2;
        private System.Windows.Forms.Label lbltxt3;
        private System.Windows.Forms.Label lbltxt6;
        private System.Windows.Forms.Label lbltxt5;
        private System.Windows.Forms.Label lbltxt4;
        private System.Windows.Forms.Label lbltxt9;
        private System.Windows.Forms.Label lbltxt8;
        private System.Windows.Forms.Label lbltxt7;
        private System.Windows.Forms.Button btnJugar;
        private System.Windows.Forms.Button btnCargarSaldo;
        private System.Windows.Forms.NumericUpDown numericUpDown0;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown numericUpDown9;
        private System.Windows.Forms.NumericUpDown numericUpDown8;
        private System.Windows.Forms.NumericUpDown numericUpDown7;
        private System.Windows.Forms.NumericUpDown numericUpDownSaldo;
    }
}