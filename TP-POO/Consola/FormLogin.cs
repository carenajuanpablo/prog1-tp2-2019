﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace Consola
{
    public partial class FormLogin : Form, IFormLogin, IObtenerDatos
    {
        Casino Casino = new Casino();

        public FormLogin()
        {
            InitializeComponent();
        }
        
        /// <summary>
        /// Se comunica con Logica.Casino. Devuelve el usuario logueado.
        /// </summary>
        /// <returns></returns>
        public Usuario ObtenerUsuarioLogueado()
        {
            return Casino.ObtenerUsuarioLogueado();
        }

        /// <summary>
        /// Verifica si el usuario se logueó correctamente.
        /// De ser asi, abre los formuliarios correspondientes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnLogin_Click(object sender, EventArgs e)
        {
            Usuario User = Casino.Login(txtEmail.Text, txtPass.Text);

            if (User == null) 
            {
                //El usuario no se logueó correctamente. 
                //User = null porque no existe en las listas de jugadores y administradores.

                MessageBox.Show("Credenciales inválidas");
            }
            else
            {
                //El usuario se logueó correctamente.

                if (User is Administrador)
                {
                    //Si el usuario es Administrador, se abre el formulario principal del administrador.

                    FormPrincipalAdmin formPrincipalAdmin = new FormPrincipalAdmin();
                    formPrincipalAdmin.Owner = this;
                    formPrincipalAdmin.ShowDialog();
                }
                else
                {
                    //Si el usuario es Jugador, se abre el formulario principal del jugador.

                    Jugador jug = User as Jugador;
                    FormPrincipalJugador formPrincipalJug = new FormPrincipalJugador();
                    formPrincipalJug.Owner = this;
                    formPrincipalJug.ShowDialog();
                }
            }
        }

        public void ActualizarSaldoLlamada(int saldo, bool movimiento)
        {
            Usuario User = ObtenerUsuarioLogueado();
            Jugador jugador = User as Jugador;
            Casino.ActualizarSaldo(saldo);
            if (movimiento == true) //setiene que crear un nuevo movimiento
            {
                Casino.CrearUnNuevoMovimiento(saldo);
            }
        }
        
        public void GuardarJugadaLlamada(int apostado, int ganado, int[] apostadoEnCadaNumero)
        {
            Usuario User = ObtenerUsuarioLogueado();
            Jugador jugador = User as Jugador;
            Casino.GuardarJugada(apostado, ganado, apostadoEnCadaNumero);
        }

        public int ObtenerSaldo()
        {
            return Casino.ObtenerSaldoJugador();
        }

        public List<Jugador> ObtenerJugadores()
        {
            return Casino.ObtenerJugadores();
        }

        public void CrearNuevoJugadorLlamada(string nombre, string email, string pass)
        {
            Casino.GuardarNuevoJugador(nombre, email, pass);
        }

        public void ModificarJugador(string nombre, string email, string pass, int codigo)
        {
            Casino.ModificarJugador(nombre, email, pass, codigo);
        }

        public Jugador ObtenerJugadorPorCodigoLlamada(int codigo)
        {
            return Casino.ObtenerJugadorPorCodigo(codigo);
        }

        public void EliminarJugador(int codigo)
        {
            Casino.EliminarJugador(codigo);
        }
    }
}