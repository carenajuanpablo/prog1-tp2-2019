﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;


namespace Consola
{
    public partial class FormPrincipalAdmin : Form, IObtenerDatos, IFormPrincipalAdmin
    {
        public FormPrincipalAdmin()
        {
            InitializeComponent();
        }

        public Usuario ObtenerUsuarioLogueado()
        {
            IObtenerDatos formLogin = this.Owner as IObtenerDatos;
            return formLogin.ObtenerUsuarioLogueado();
        }
        
        public List<Jugador> ObtenerJugadores()
        {
            IObtenerDatos formLogin = this.Owner as IObtenerDatos;
            return formLogin.ObtenerJugadores();
        }

        private void jugadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormJugadas formJugadas = new FormJugadas();
            formJugadas.Owner = this;
            formJugadas.ShowDialog();
        }
        
        public void Form1_Load(object sender, EventArgs e)
        {
            List<Jugador> jugadores = new List<Jugador>();
            IObtenerDatos formLogin = this.Owner as IObtenerDatos;
            jugadores = formLogin.ObtenerJugadores();

            DataTable dt = new DataTable();
            dt.Columns.Add("Código");
            dt.Columns.Add("Nombre");
            dt.Columns.Add("Email");
            dt.Columns.Add("Password");

            foreach (var oItem in jugadores)
            {
                dt.Rows.Add(new object[] { oItem.Codigo, oItem.Nombre, oItem.Email, oItem.Pass });
            }

            gridJug.DataSource = dt;
        }

        private void gridJugadores_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var column = gridJug.Columns[e.ColumnIndex];
            var row = gridJug.Rows[e.RowIndex];

            //En column podemos encontrar el nombre de la columna que definimos en el diseñador.

            if (column.Name == "Eliminar")
            {
                var mensaje = MessageBox.Show("Está seguro que desea eliminar el jugador?", "Eliminar jugador", MessageBoxButtons.OKCancel);

                if (mensaje == DialogResult.OK)
                {
                    int codigo = int.Parse(gridJug.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value.ToString());
                    IFormLogin formLogin = this.Owner as IFormLogin;
                    formLogin.EliminarJugador(codigo);
                    Form1_Load(sender, e);
                }
            }

            if (column.Name == "Modificar")
            {
                int codigo = int.Parse(gridJug.Rows[e.RowIndex].Cells[e.ColumnIndex + 2].Value.ToString());
                IFormLogin formLogin = this.Owner as IFormLogin;
                Jugador jugador = formLogin.ObtenerJugadorPorCodigoLlamada(codigo);

                FormNuevoJugador formNuevoJugador = new FormNuevoJugador(false, jugador);
                formNuevoJugador.Owner = this;
                formNuevoJugador.ShowDialog();
            }
        }

        /// <summary>
        /// Abre el formulario FormNuevoJugador para crear un nuevo Jugador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            FormNuevoJugador formNuevoJugador = new FormNuevoJugador(true);
            formNuevoJugador.Owner = this;
            formNuevoJugador.ShowDialog();
        }
        
        public void CrearNuevoJugadorLlamada(string nombre, string email, string pass)
        {
            IFormLogin formLogin = Owner as IFormLogin;
            formLogin.CrearNuevoJugadorLlamada(nombre, email, pass);
        }

        public void ModificarJugador(string nombre, string email, string pass, int codigo)
        {
            IFormLogin formLogin = Owner as IFormLogin;
            formLogin.ModificarJugador(nombre, email, pass, codigo);
        }
    }
}