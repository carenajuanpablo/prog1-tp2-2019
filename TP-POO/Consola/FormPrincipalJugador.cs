﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace Consola
{
    public partial class FormPrincipalJugador : Form, IObtenerDatos
    {
        public FormPrincipalJugador()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Cuando carga el formulario, carga el lblSaldo.Value con el saldo actual del jugador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Form1_load(object sender, EventArgs e)
        {
            IFormLogin formLogin = Owner as IFormLogin;
            int saldo = formLogin.ObtenerSaldo();
            lblSaldo.Text = Convert.ToString(saldo);
        }

        /// <summary>
        /// Devuelve una lista de Usuarios existentes.
        /// </summary>
        /// <returns></returns>
        public List<Jugador> ObtenerJugadores()
        {
            IObtenerDatos formLogin = this.Owner as IObtenerDatos;
            return formLogin.ObtenerJugadores();
        }
        public Usuario ObtenerUsuarioLogueado()
        {
            IObtenerDatos formLogin = this.Owner as IObtenerDatos;
            return formLogin.ObtenerUsuarioLogueado();
        }

        private void btnCargarSaldo_Click(object sender, EventArgs e)
        {
            IFormLogin formLogin = this.Owner as IFormLogin;
            formLogin.ActualizarSaldoLlamada(Convert.ToInt32(numericUpDownSaldo.Value), true); //True porque se tiene que generar un movimiento
            lblSaldo.Text = Convert.ToString(Convert.ToInt32(lblSaldo.Text) + Convert.ToInt32(numericUpDownSaldo.Value));
            numericUpDownSaldo.Value = 0;
        }

        private void jugadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormJugadas formJugadas = new FormJugadas();
            formJugadas.Owner = this;
            formJugadas.ShowDialog();
        }

        private void btnJugar_Click(object sender, EventArgs e)
        {
            int apostado = CantidadApostada(); //Suma la cantidad que apostó el jugador
            bool apuesta = VerificarSiSePuedeApostar(apostado); //Verifica que el saldo sea mayor igual a lo apostado

            if (apuesta == true) // Si se cumple la condicion anterior
            {
                int numRandom = GenerarNumeroRandom(); //Genera un numero random

                int[] apostadoEnCadaNumero = GenerarArrayDeApuesta(); //Crea una vector con lo apostado en cada numero

                int ganado = CalcularResultadoApuesta(apostadoEnCadaNumero, numRandom); //Calcula lo ganado o lo perdido

                IFormLogin formLogin = this.Owner as IFormLogin; //se comunica con la interfaz
                formLogin.GuardarJugadaLlamada(apostado, ganado, apostadoEnCadaNumero); //Llama (en la interfaz) para guardar la jugada
                formLogin.ActualizarSaldoLlamada(ganado, false); //Llama para actualizar el saldo del jugador //False porque no tiene que generar un movimiento

                MostrarJugada(numRandom, apostado, ganado); //muestra un dialogo que dará el resumen de la jugada
            }
            else
            {
                if (apostado == 0)
                {
                    MessageBox.Show("Debe apostar algo");
                }
                else
                {
                    MessageBox.Show("Saldo Insuficiente");
                }
            }

            ActualizarPantallaDelJugador();
        }

        private void ActualizarPantallaDelJugador()
        {
            IFormLogin formLogin = this.Owner as IFormLogin; //se comunica con la interfaz
            lblSaldo.Text = formLogin.ObtenerSaldo().ToString(); //actualiza el lblSaldo
            
            //poner los numericUpDown en 0
            numericUpDown0.Value = 0;
            numericUpDown1.Value = 0;
            numericUpDown2.Value = 0;
            numericUpDown3.Value = 0;
            numericUpDown4.Value = 0;
            numericUpDown5.Value = 0;
            numericUpDown6.Value = 0;
            numericUpDown7.Value = 0;
            numericUpDown8.Value = 0;
            numericUpDown9.Value = 0;
        }

        private int[] GenerarArrayDeApuesta()
        {
            int[] apostadoEnCadaNumero = new int[10];
            apostadoEnCadaNumero[0] = Convert.ToInt32(numericUpDown0.Value);
            apostadoEnCadaNumero[1] = Convert.ToInt32(numericUpDown1.Value);
            apostadoEnCadaNumero[2] = Convert.ToInt32(numericUpDown2.Value);
            apostadoEnCadaNumero[3] = Convert.ToInt32(numericUpDown3.Value);
            apostadoEnCadaNumero[4] = Convert.ToInt32(numericUpDown4.Value);
            apostadoEnCadaNumero[5] = Convert.ToInt32(numericUpDown5.Value);
            apostadoEnCadaNumero[6] = Convert.ToInt32(numericUpDown6.Value);
            apostadoEnCadaNumero[7] = Convert.ToInt32(numericUpDown7.Value);
            apostadoEnCadaNumero[8] = Convert.ToInt32(numericUpDown8.Value);
            apostadoEnCadaNumero[9] = Convert.ToInt32(numericUpDown9.Value);

            return apostadoEnCadaNumero;
        }

        private int CalcularResultadoApuesta(int[] _apostadoEnCadaNumero, int numRandom)
        {
            int ganado = 0;

            for (int i = 0; i < _apostadoEnCadaNumero.Length; i++) //recorre todo el vector
            {
                if (i != numRandom) //mientras que i sea distinto al numero sorteado
                {
                    ganado -= _apostadoEnCadaNumero[i]; //se resta lo apostado en i (porque no salió sorteado)
                }
                else // si i == numRandom
                {
                    ganado += _apostadoEnCadaNumero[i] * 3; // se suma lo apostado en el numero sorteado x3
                }
            }

            return ganado;
        }

        private void MostrarJugada(int _numRandom, int _apostado, int _ganado)
        {
            MessageBox.Show("Numero Sorteado: " + _numRandom + " | Total apostado: " + _apostado + " | Resultado: " + _ganado);
        }

        private int GenerarNumeroRandom()
        {
            Random newRandom = new Random();
            int numRandom = newRandom.Next(10); //Crea un numero random entre el 0 y el 9
            return numRandom;
        }

        private int CantidadApostada()
        {
            int cantidadApostada = Convert.ToInt32(numericUpDown0.Value) + Convert.ToInt32(numericUpDown1.Value) +
                Convert.ToInt32(numericUpDown2.Value) + Convert.ToInt32(numericUpDown3.Value) +
                Convert.ToInt32(numericUpDown4.Value) + Convert.ToInt32(numericUpDown5.Value) +
                Convert.ToInt32(numericUpDown6.Value) + Convert.ToInt32(numericUpDown7.Value) +
                Convert.ToInt32(numericUpDown8.Value) + Convert.ToInt32(numericUpDown9.Value);

            return cantidadApostada;
        }

        private bool VerificarSiSePuedeApostar(int cantidadApostada)
        {
            //Verifica si lo apostado es menor o igual al saldo

            int saldo = Convert.ToInt32(lblSaldo.Text);
            
            if (cantidadApostada <= saldo && cantidadApostada != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}