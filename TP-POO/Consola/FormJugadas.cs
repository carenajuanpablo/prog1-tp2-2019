﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace Consola
{
    public partial class FormJugadas : Form
    {
        public FormJugadas()
        {
            InitializeComponent();
        }

        public void FormJugadas_Load(object sender, EventArgs e)
        {
            IObtenerDatos formPrincipalAdmin = this.Owner as IObtenerDatos;
            Usuario User = formPrincipalAdmin.ObtenerUsuarioLogueado();

            bool esJug = User is Jugador;

            if (esJug)
            {
                cmbJug.Enabled = false;
                btnReporteGanancias.Enabled = false;

                Jugador jugador = User as Jugador;

                ActualizarGrilla(jugador);
            }
            else
            {
                List<Jugador> jugadores = new List<Jugador>();

                jugadores.Add(new Jugador() { Codigo = 0, Nombre = "Seleccione..." });

                jugadores.AddRange(formPrincipalAdmin.ObtenerJugadores());

                cmbJug.DataSource = jugadores;
                cmbJug.DisplayMember = "Nombre";
                cmbJug.ValueMember = "Codigo";

                ActualizarGrilla(null);
            }
        }
        
        private void ActualizarGrilla(Jugador jugador)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Jugador"); ///
            dt.Columns.Add("Monto Apostado");
            dt.Columns.Add("Monto Ganado");
            dt.Columns.Add("Fecha");

            if (jugador!=null && jugador.Nombre != "Seleccione...")
            {
                foreach (var item in jugador.ListaDeJugadas)
                {
                    dt.Rows.Add(new object[] { jugador.Nombre, item.MontoApostado, item.MontoGanado, item.Fecha }); ///
                }
            }
            else
            {
                IObtenerDatos formPrincipalAdmin = this.Owner as IObtenerDatos;
                List<Jugador> jugadores = new List<Jugador>();
                jugadores.AddRange(formPrincipalAdmin.ObtenerJugadores());

                foreach (var us in jugadores)
                {
                    Jugador jug = us as Jugador;
                    if (jug != null)
                    {
                        foreach (var item in jug.ListaDeJugadas)
                        {
                            dt.Rows.Add(new object[] { jug.Nombre, item.MontoApostado, item.MontoGanado, item.Fecha }); ///
                        }
                    }
                    
                }
            }

            dgvJugadas.DataSource = dt;
        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            IObtenerDatos formPrincipalAdmin = this.Owner as IObtenerDatos;

            Usuario User = formPrincipalAdmin.ObtenerUsuarioLogueado();

            Jugador Jug = User as Jugador;
            DataTable dt = new DataTable();
            dt.Columns.Add("Jugador"); ///
            dt.Columns.Add("Monto Apostado");
            dt.Columns.Add("Monto Ganado");
            dt.Columns.Add("Fecha");

            if (Jug != null)
            {
                foreach (var item in Jug.ListaDeJugadas)
                {
                    if ((dtpDesde.Value.Date <= item.Fecha) && (dtpHasta.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59) >= item.Fecha))
                    {
                        dt.Rows.Add(new object[] { Jug.Nombre, item.MontoApostado, item.MontoGanado, item.Fecha }); ///
                    }
                }
            }
            else
            {
                if (cmbJug.Text == "Seleccione...")
                {
                    List<Jugador> jugadores = new List<Jugador>();
                    jugadores.AddRange(formPrincipalAdmin.ObtenerJugadores());

                    foreach (var us in jugadores)
                    {
                        foreach (var item in us.ListaDeJugadas)
                        {
                            if ((dtpDesde.Value.Date <= item.Fecha) && (dtpHasta.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59) >= item.Fecha))
                                dt.Rows.Add(new object[] { us.Nombre, item.MontoApostado, item.MontoGanado, item.Fecha }); ///
                        }
                    }
                }
                else
                {
                    List<Jugador> jugadores = cmbJug.DataSource as List<Jugador>;
                    
                    foreach (var jugador in jugadores)
                    {
                        if (cmbJug.Text == jugador.Nombre)
                        {
                            foreach (var item in jugador.ListaDeJugadas)
                            {
                                if ((dtpDesde.Value.Date <= item.Fecha) && (dtpHasta.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59) >= item.Fecha))
                                {
                                    dt.Rows.Add(new object[] { jugador.Nombre, item.MontoApostado, item.MontoGanado, item.Fecha }); ///
                                }
                            }
                        }
                    }
                }
            }
            
            if (dt.Rows.Count > 0)
            {
                dgvJugadas.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No se han identificado Jugadas para el filtro indicado");
            }
        }

        private void btnReporteGanancias_Click(object sender, EventArgs e)
        {
            int cantidadApostada = 0;
            int cantidadGanada = 0;

            for (int i = 0; i < dgvJugadas.RowCount-1; i++)
            {
                cantidadApostada += int.Parse(dgvJugadas.Rows[i].Cells[1].Value.ToString());
                cantidadGanada += int.Parse(dgvJugadas.Rows[i].Cells[2].Value.ToString());
            }

            MessageBox.Show("Cantidad Apostada por los jugadores: " + cantidadApostada + " | Cantidad Ganada por el Casino: " + -cantidadGanada);
        }

        private void cmbJug_SelectedIndexChanged(object sender, EventArgs e)
        {
            Jugador jugadorSeleccionado = cmbJug.SelectedItem as Jugador;

            ActualizarGrilla(jugadorSeleccionado);
        }
    }
}