﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Consola
{
    interface IFormLogin
    {
        /// <summary>
        /// Se comunica con FormLogin. Este a su vez se comunica con Logica.Casino para actualizar el saldo del Jugador logueado.
        /// </summary>
        /// <param name="saldo"></param>
        /// <param name="movimiento">Es un booleano que indica si se debe cargar un movimiento o no.</param>
        void ActualizarSaldoLlamada(int saldo, bool movimiento);

        /// <summary>
        /// Se comunica con FormLogin. Este a su vez se comunica con Logica.Casino para obtener el saldo del Jugador logueado.
        /// </summary>
        /// <returns></returns>
        int ObtenerSaldo();

        /// <summary>
        /// Se comunica con FormLogin. Este a su vez se comunica con Logica.Casino para Guardar una jugada.
        /// </summary>
        /// <param name="apostado"></param>
        /// <param name="ganado"></param>
        /// <param name="apostadoEnCadaNumero"></param>
        void GuardarJugadaLlamada(int apostado, int ganado, int[] apostadoEnCadaNumero);

        void CrearNuevoJugadorLlamada(string nombre, string email, string pass);

        void ModificarJugador(string nombre, string email, string pass, int codigo);

        Jugador ObtenerJugadorPorCodigoLlamada(int codigo);

        void EliminarJugador(int codigo);
    }  
}