﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace Consola
{
    public partial class FormNuevoJugador : Form
    {
        bool Crear;
        Jugador Jugador = new Jugador();

        public FormNuevoJugador(bool crear)
        {
            InitializeComponent();
            Crear = crear;
        }

        public FormNuevoJugador(bool crear, Jugador jugador)
        {
            InitializeComponent();
            Crear = crear;
            Jugador = jugador;
            txtNombre.Text = jugador.Nombre;
            txtEmail.Text = jugador.Email;
            txtPassword.Text = jugador.Pass;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            IFormPrincipalAdmin formPrincipalAdmin = Owner as IFormPrincipalAdmin;
            if (Crear)
            {
                formPrincipalAdmin.CrearNuevoJugadorLlamada(Convert.ToString(txtNombre.Text), Convert.ToString(txtEmail.Text), Convert.ToString(txtPassword.Text));
            }
            else
            {
                formPrincipalAdmin.ModificarJugador(Convert.ToString(txtNombre.Text), Convert.ToString(txtEmail.Text), Convert.ToString(txtPassword.Text), Jugador.Codigo);
            }
            
            Close();
            formPrincipalAdmin.Form1_Load(sender, e);
        }
    }
}