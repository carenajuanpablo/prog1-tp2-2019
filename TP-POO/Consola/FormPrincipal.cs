﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Consola
{
    public partial class FormPrincipal : Form
    {
        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void nuevoJugadorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormNuevoJugador formNew = new FormNuevoJugador();
            formNew.Owner = this;
            formNew.ShowDialog();

            //NUEVO COMENTARIO
        }
    }
}
