﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consola
{
    interface IFormPrincipalAdmin
    {
        void CrearNuevoJugadorLlamada(string nombre, string email, string pass);
        void Form1_Load(object sender, EventArgs e);
        void ModificarJugador(string nombre, string email, string pass, int codigo);
    }
}